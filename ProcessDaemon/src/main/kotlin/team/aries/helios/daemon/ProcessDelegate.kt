package team.aries.helios.daemon

import io.netty.bootstrap.Bootstrap
import io.netty.channel.Channel
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInitializer
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.epoll.EpollDomainSocketChannel
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.unix.DomainSocketAddress
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.Delimiters
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import io.netty.handler.timeout.ReadTimeoutHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class ProcessDelegate {

    private val scheduler = Executors.newSingleThreadScheduledExecutor()
    private val future = CompletableFuture<Unit>()
    private var processRunner = ProcessRunner(future)
    private val semaphore = Semaphore()
    private val eventLoop = EpollEventLoopGroup()

    fun init(socket: String, entryPoint: String, external: Boolean, id: String, persistent: Boolean) {
        Thread {
            val systemIn = System.`in`.bufferedReader()
            while(true) {
                val line = systemIn.readLine() ?: break
                if(line == "!!!stop") {
                    processRunner.forceStop = true
                    processRunner.end()
                    future.complete(Unit)
                    break
                }
                processRunner.writeToProcess(line)
            }
        }.start()
        Runtime.getRuntime().addShutdownHook(Thread {
            processRunner.end()
        })
        processRunner.init(entryPoint, external, persistent, semaphore)
        connect(socket, entryPoint, external, id, persistent)
        future.get()
        eventLoop.shutdownGracefully()
    }

    fun connect(socket: String, entryPoint: String, external: Boolean, id: String, persistent: Boolean) {
        Thread {
            Bootstrap()
                .channel(EpollDomainSocketChannel::class.java)
                .group(eventLoop)
                .handler(object: ChannelInitializer<Channel>() {
                    override fun initChannel(chn: Channel) {
                        chn.pipeline().apply {
                            addLast(DelimiterBasedFrameDecoder(1024, *Delimiters.nulDelimiter()))
                            addLast(StringDecoder())
                            addLast(StringEncoder())
                            addLast(object: SimpleChannelInboundHandler<String>() {
                                override fun channelActive(ctx: ChannelHandlerContext) {
                                    this@ProcessDelegate.processRunner.ctx = ctx
                                    println("Connected to Coordinator")
                                    if(external) {
                                        ctx.channel().writeAndFlush("EXTERNAL $id\u0000")
                                    } else {
                                        ctx.channel().writeAndFlush("CONNECT $id\u0000")
                                    }
                                }

                                override fun channelRead0(ctx: ChannelHandlerContext, str: String) {
                                    var string = str
                                    if(string == "START") {
                                        semaphore.go()
                                        ctx.writeAndFlush("STARTING\u0000")
                                        return
                                    }
                                    if(string == "RESTART" && persistent) {
                                        if(processRunner.running) {
                                            processRunner.end()
                                        }
                                        ctx.writeAndFlush("STARTING\u0000")
                                        processRunner.init(entryPoint, external, persistent, semaphore)
                                    }
                                    if(string == "END" || string == "KILL") {
                                        if(string == "KILL") {
                                            processRunner.forceStop = true
                                        }
                                        processRunner.end()
                                        if(string == "KILL") {
                                            future.complete(Unit)
                                            System.exit(137)
                                        }
                                        return
                                    }
                                    if(string.startsWith("IN ")) {
                                        string = string.drop(3)
                                        println(string)
                                        processRunner.writeToProcess(string)
                                    }
                                }

                                override fun channelInactive(ctx: ChannelHandlerContext) {
                                    println("Lost connection to Coordinator, retrying in 2 seconds")
                                    scheduler.schedule({
                                        this@ProcessDelegate.connect(socket, entryPoint, external, id, persistent)
                                    }, 2000, TimeUnit.MILLISECONDS)
                                }

                                override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
                                    cause?.printStackTrace()
                                }
                            })
                        }
                    }
                })
                .connect(DomainSocketAddress(socket))
                .addListener {
                    if(!it.isSuccess) {
                        println("Could not connect to Coordinator, retrying in 2 seconds")
                        scheduler.schedule({
                            this@ProcessDelegate.connect(socket, entryPoint, external, id, persistent)
                        }, 2000, TimeUnit.MILLISECONDS)
                    }
                }
                .channel().closeFuture().sync()
        }.start()
    }

}