package team.aries.helios.daemon

import io.netty.channel.ChannelHandlerContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.util.concurrent.CompletableFuture

class ProcessRunner(private val future: CompletableFuture<Unit>) {

    private var process: Process? = null
    var ctx: ChannelHandlerContext? = null
    var running = false
    var forceStop = false

    fun init(entryPoint: String, external: Boolean, persistent: Boolean, semaphore: Semaphore?) {
        if(running) return
        running = true
        Thread {
            semaphore?.waitFor()
            println("Process starting ${Thread.currentThread().name}")
            ProcessBuilder(*entryPoint.trim().split(" ").toTypedArray())
                .directory(File("."))
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectInput(ProcessBuilder.Redirect.PIPE)
                .start()
                .also {
                    this@ProcessRunner.process = it
                    ctx?.writeAndFlush("RUNNING\u0000")
                    Thread {
                        it.inputStream.bufferedReader().apply {
                            while(true) {
                                try {
                                    val line = this.readLine() ?: if(!persistent && !forceStop) {
                                        break
                                    } else {
                                        continue
                                    }
                                    if(external) {
                                        println(line)
                                    }
                                    ctx?.writeAndFlush("OUT $line\u0000")
                                } catch(ignored: Exception) {}
                            }
                        }
                        it.errorStream.bufferedReader().apply {
                            while(true) {
                                try {
                                    val line = this.readLine() ?: if(!persistent && !forceStop) {
                                        break
                                    } else {
                                        continue
                                    }
                                    if(external) {
                                        println(line)
                                    }
                                    ctx?.writeAndFlush("ERROR $line\u0000")
                                } catch(ignored: Exception) {}
                            }
                        }
                    }.start()
                }.waitFor().apply {
                    ctx?.writeAndFlush("NOT_RUNNING\u0000")
                    println("Process ended ${Thread.currentThread().name} exit code :: $this")
                }
            if(persistent && !forceStop) {
                if(RESTART) {
                    ctx?.writeAndFlush("STARTING\u0000")
                    init(entryPoint, external, persistent, null)
                } else {
                    running = false
                }
            } else {
                end()
                future.complete(Unit)
            }
        }.apply {
            this.name = "process::${System.currentTimeMillis()}"
            this.isDaemon = true
            this.start()
        }
    }

    fun writeToProcess(string: String) {
        process?.outputStream?.write("$string\n".toByteArray())
        process?.outputStream?.flush()
    }

    fun end() {
        process?.destroyForcibly()?.waitFor()
    }

}