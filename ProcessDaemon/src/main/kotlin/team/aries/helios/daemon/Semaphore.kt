package team.aries.helios.daemon

import java.util.concurrent.CompletableFuture

class Semaphore {

    private val future = CompletableFuture<Unit>()

    fun waitFor() = future.get()!!

    fun go() { if(!future.isDone) {
        future.complete(Unit)
    }}

}