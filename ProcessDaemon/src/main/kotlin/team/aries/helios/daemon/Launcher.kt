package team.aries.helios.daemon

import java.io.File

var RESTART = false

fun main(args: Array<String>) {

    val socket = if(args.indexOf("-socket") != -1 && args.size > 1) {
        args[args.indexOf("-socket") + 1]
    } else {
        "coordinator.sock"
    }

    val entryPoint = File(".entrypoint").readText()

    val external = args.indexOf("-external") != -1

    val persistent = args.indexOf("-pst") != -1

    RESTART = args.indexOf("-rst") != -1

    val pid = if(args.indexOf("-pid") != -1 && args.size > 1) {
        args[args.indexOf("-pid") + 1]
    } else {
        "none"
    }

    if(entryPoint == "none") {
        System.exit(-1)
    }

    println("Socket: $socket")
    println("Entry-Point: $entryPoint")
    println("External: $external")
    println("PID: $pid")
    println("Persistent: $persistent")

    ProcessDelegate()
        .init(socket, entryPoint, external, pid, persistent)
}