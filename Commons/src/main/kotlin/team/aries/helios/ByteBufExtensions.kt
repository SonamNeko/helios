package team.aries.helios

import io.netty.buffer.ByteBuf
import io.netty.util.CharsetUtil

fun ByteBuf.readString(): String {
    val len = this.readInt()
    return this.readCharSequence(len, CharsetUtil.UTF_8).toString()
}

fun ByteBuf.writeString(string: String) {
    this.writeInt(string.length)
    this.writeCharSequence(string, CharsetUtil.UTF_8)
}

fun ByteBuf.readByteArray(): ByteArray {
    val len = this.readInt()
    val bytes = ByteArray(len)
    this.readBytes(bytes)
    return bytes
}

fun ByteBuf.writeByteArray(byteArray: ByteArray) {
    this.writeInt(byteArray.size)
    this.writeBytes(byteArray)
}