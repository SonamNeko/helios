package team.aries.helios.netty

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToByteEncoder
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey

class AESEncoder(private val key: SecretKey) : MessageToByteEncoder<ByteBuf>() {

    private val cipher = Cipher.getInstance("AES")

    override fun encode(ctx: ChannelHandlerContext, incoming: ByteBuf, out: ByteBuf) {
        cipher.init(Cipher.ENCRYPT_MODE, key)
        val bytes = ByteArray(incoming.readableBytes())
        incoming.readBytes(bytes)
        val encodedBytes = cipher.doFinal(bytes)
        out.writeBytes(encodedBytes)
    }

}