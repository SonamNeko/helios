package team.aries.helios.netty

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToByteEncoder
import team.aries.helios.api.HeliosPacket

class HeliosEncoder : MessageToByteEncoder<HeliosPacket>() {

    override fun encode(ctx: ChannelHandlerContext, packet: HeliosPacket, buf: ByteBuf) {
        buf.writeInt(packet.id)
        packet.write(buf)
    }

}