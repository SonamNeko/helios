package team.aries.helios.netty

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToMessageDecoder
import io.netty.util.CharsetUtil
import team.aries.helios.Protocol

class HeliosDecoder : MessageToMessageDecoder<ByteBuf>() {

    override fun decode(ctx: ChannelHandlerContext, buf: ByteBuf, out: MutableList<Any>) {
        val id = buf.readInt()
        Protocol.Packet.PacketMapping[id]?.let {
            val packet = it()
            packet.read(buf)
            out.add(packet)
        }
    }

}