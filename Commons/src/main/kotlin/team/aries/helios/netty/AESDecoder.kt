package team.aries.helios.netty

import io.netty.buffer.ByteBuf
import io.netty.buffer.ByteBufAllocator
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ByteToMessageDecoder
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey

class AESDecoder(private val key: SecretKey) : ByteToMessageDecoder() {

    private val cipher = Cipher.getInstance("AES")

    override fun decode(ctx: ChannelHandlerContext, buffer: ByteBuf, out: MutableList<Any>) {
        val bytes = ByteArray(buffer.readableBytes())
        buffer.readBytes(bytes)
        cipher.init(Cipher.DECRYPT_MODE, key)
        val decoded = cipher.doFinal(bytes)
        val decodedBuffer = ByteBufAllocator.DEFAULT.directBuffer()
        decodedBuffer.writeBytes(decoded)
        out.add(decodedBuffer)
    }
}