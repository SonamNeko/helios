package team.aries.helios.packet.coordbound

import io.netty.buffer.ByteBuf
import org.apache.commons.lang3.RandomStringUtils
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString

class ProvisionRequest() : HeliosPacket {

    lateinit var ackId: String
    lateinit var provision: Provision

    constructor(provision: Provision): this() {
        this.ackId = RandomStringUtils.randomAlphanumeric(16)
        this.provision = provision
    }

    override fun read(buffer: ByteBuf) {
        ackId = buffer.readString()
        val readProvision = Provision()
        readProvision.read(buffer)
        provision = readProvision
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(ackId)
        provision.write(buffer)
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int = Protocol.Packet.TYPE_PROVISION_REQ
}