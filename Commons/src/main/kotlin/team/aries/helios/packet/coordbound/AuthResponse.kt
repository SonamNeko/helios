package team.aries.helios.packet.coordbound

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.JsonObject
import io.netty.buffer.ByteBuf
import team.aries.helios.*
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket

class AuthResponse(var success: Boolean = false, var config: JsonObject = JsonObject()) : HeliosPacket {

    override fun read(buffer: ByteBuf) {
        success = buffer.readBoolean()
        config = HeliosPacket.Mapper.fromJson(buffer.readString())
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeBoolean(success)
        buffer.writeString(HeliosPacket.Mapper.toJson(config))
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_AUTHRES
}