package team.aries.helios.packet

import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readByteArray
import team.aries.helios.writeByteArray
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import java.util.concurrent.ThreadLocalRandom

class Negotiate() : HeliosPacket {

    private val keyFactory = KeyFactory.getInstance("RSA")

    lateinit var publicKey: PublicKey
    var meetsCriteria = false

    constructor(publicKey: PublicKey): this() {
        this.publicKey = publicKey
    }

    override fun read(buffer: ByteBuf) {
        try {
            buffer.readByteArray().reversedArray()
            val bytes = buffer.readByteArray().reversedArray()
            val spec = X509EncodedKeySpec(bytes)
            val pub = keyFactory.generatePublic(spec)
            publicKey = pub
            meetsCriteria = true
        } catch (ex: Exception) {
            buffer.discardReadBytes()
        }
    }

    override fun write(buffer: ByteBuf) {
        val discardable = ByteArray(128)
        ThreadLocalRandom.current().nextBytes(discardable)
        buffer.writeByteArray(discardable)
        buffer.writeByteArray(publicKey.encoded.reversedArray())
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_NEGOTIATE
}