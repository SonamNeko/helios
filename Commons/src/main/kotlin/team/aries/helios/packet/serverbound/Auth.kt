package team.aries.helios.packet.serverbound

import io.netty.buffer.ByteBuf
import team.aries.helios.*
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import java.util.*

class Auth() : HeliosPacket {

    lateinit var authKey: ByteArray
    lateinit var name: String

    constructor(authKey: ByteArray, name: String): this() {
        this.authKey = authKey
        this.name = name
    }

    override fun read(buffer: ByteBuf) {
        authKey = buffer.readByteArray()
        name = buffer.readString()
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeByteArray(authKey)
        buffer.writeString(name)
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_AUTH
}