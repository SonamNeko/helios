package team.aries.helios.packet.coordbound

import com.github.salomonbrys.kotson.fromJson
import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString

class Provision() : HeliosPacket {

    lateinit var serverName: String
    lateinit var `package`: String
    lateinit var environment: List<String>
    lateinit var variables: Map<String, String>

    constructor(serverName: String, `package`: String, environment: List<String>, variables: Map<String, String>): this() {
        this.serverName = serverName
        this.`package` = `package`
        this.environment = environment
        this.variables = variables
    }

    override fun read(buffer: ByteBuf) {
        serverName = buffer.readString()
        `package` = buffer.readString()
        environment = Protocol.MAPPER.fromJson(buffer.readString())
        variables = Protocol.MAPPER.fromJson(buffer.readString())
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(serverName)
        buffer.writeString(`package`)
        buffer.writeString(Protocol.MAPPER.toJson(environment))
        buffer.writeString(Protocol.MAPPER.toJson(variables))
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_PROVISION

}