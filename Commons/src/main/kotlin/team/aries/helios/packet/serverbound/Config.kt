package team.aries.helios.packet.serverbound

import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString

class Config : HeliosPacket {

    lateinit var hostTunnel: String
    var          portTunnel: Int = 0

    override fun read(buffer: ByteBuf) {
        hostTunnel = buffer.readString()
        portTunnel = buffer.readInt()
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(hostTunnel)
        buffer.writeInt(portTunnel)
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_CONFIG
}