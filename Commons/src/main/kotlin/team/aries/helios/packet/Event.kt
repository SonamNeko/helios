package team.aries.helios.packet

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosEventData
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString

class Event() : HeliosPacket {

    lateinit var channel: String
    var data: JsonElement = JsonObject()

    constructor(channel: String, any: Any): this() {
        this.channel = channel
        this.data = Protocol.MAPPER.toJsonTree(any)
    }

    constructor(data: HeliosEventData): this(data.channel, data)

    override fun read(buffer: ByteBuf) {
        channel = buffer.readString()
        data = Protocol.MAPPER.fromJson(buffer.readString())
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(channel)
        buffer.writeString(Protocol.MAPPER.toJson(data))
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_EVENT
}