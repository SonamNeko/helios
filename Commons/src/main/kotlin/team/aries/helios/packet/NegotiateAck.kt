package team.aries.helios.packet

import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readByteArray
import team.aries.helios.writeByteArray
import java.security.PrivateKey
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

class NegotiateAck(var bound: Boolean = false, var key: ByteArray = ByteArray(0)) : HeliosPacket {

    override fun read(buffer: ByteBuf) {
        this.bound = buffer.readBoolean()
        key = buffer.readByteArray()
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeBoolean(bound)
        buffer.writeByteArray(Base64.getEncoder().encode(key))
    }

    fun decryptKey(privateKey: PrivateKey): SecretKey {
        val cipher = Cipher.getInstance("RSA")
        cipher.init(Cipher.DECRYPT_MODE, privateKey)
        val decrypted = cipher.doFinal(Base64.getDecoder().decode(this.key))
        return SecretKeySpec(decrypted, 0, decrypted.size, "AES")
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_NEGOTIATE_ACK
}