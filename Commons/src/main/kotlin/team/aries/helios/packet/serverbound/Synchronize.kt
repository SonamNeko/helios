package team.aries.helios.packet.serverbound

import com.github.salomonbrys.kotson.fromJson
import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.CoordProcess
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString

class Synchronize(var processes: List<CoordProcess> = mutableListOf()) : HeliosPacket {



    override fun read(buffer: ByteBuf) {
        processes = HeliosPacket.Mapper.fromJson(buffer.readString())
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(HeliosPacket.Mapper.toJson(processes))
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_SYNCHRONIZE
}