package team.aries.helios.packet

import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString

class Output() : HeliosPacket {

    lateinit var text: String

    constructor(text: String): this() {
        this.text = text
    }

    override fun read(buffer: ByteBuf) {
        text = buffer.readString()
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(text)
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_OUTPUT
}