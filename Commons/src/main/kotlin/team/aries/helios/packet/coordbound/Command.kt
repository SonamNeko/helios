package team.aries.helios.packet.coordbound

import com.github.salomonbrys.kotson.fromJson
import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString

class Command
    (
    var label: String = "unknown",
    var args: List<String> = listOf()
): HeliosPacket {

    override fun read(buffer: ByteBuf) {
        label = buffer.readString()
        args = HeliosPacket.Mapper.fromJson(buffer.readString())
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(label)
        buffer.writeString(HeliosPacket.Mapper.toJson(args))
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int = Protocol.Packet.TYPE_COMMAND
}