package team.aries.helios.packet.serverbound

import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket
import team.aries.helios.readString
import team.aries.helios.writeString
import java.util.UUID

class ProvisionResponse() : HeliosPacket {

    lateinit var uuid: UUID
    lateinit var name: String
    lateinit var ackId: String

    constructor(ackId: String, name: String, uuid: UUID): this() {
        this.ackId = ackId
        this.name = name
        this.uuid = uuid
    }

    override fun read(buffer: ByteBuf) {
        uuid = UUID.fromString(buffer.readString())
        name = buffer.readString()
        ackId = buffer.readString()
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeString(uuid.toString())
        buffer.writeString(name)
        buffer.writeString(ackId)
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_PROVISIONRES
}