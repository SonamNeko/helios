package team.aries.helios.packet.serverbound

import io.netty.buffer.ByteBuf
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosPacket

class Heartbeat(var uptime: Long = -1) : HeliosPacket {


    override fun read(buffer: ByteBuf) {
        uptime = buffer.readLong()
    }

    override fun write(buffer: ByteBuf) {
        buffer.writeLong(uptime)
    }

    override fun handle(handler: AbstractPacketHandler) {
        handler.handle(this)
    }

    override val id: Int
        get() = Protocol.Packet.TYPE_HEARTBEAT
}