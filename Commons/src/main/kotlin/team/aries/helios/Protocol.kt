package team.aries.helios

import com.google.gson.Gson
import team.aries.helios.packet.*
import team.aries.helios.packet.coordbound.AuthResponse
import team.aries.helios.packet.coordbound.Command
import team.aries.helios.packet.coordbound.Provision
import team.aries.helios.packet.coordbound.ProvisionRequest
import team.aries.helios.packet.serverbound.*

object Protocol {

    val MAPPER = Gson()

    object Packet {
        const val TYPE_AUTH         = 0x0
        const val TYPE_AUTHRES      = 0x1
        const val TYPE_CONFIG       = 0x2
        const val TYPE_SYNCHRONIZE  = 0x3
        const val TYPE_HEARTBEAT    = 0x4
        const val TYPE_INPUT        = 0x5
        const val TYPE_OUTPUT       = 0x6
        const val TYPE_EVENT        = 0x7
        const val TYPE_PROVISION    = 0x8
        const val TYPE_PROVISIONRES    = 0x9
        const val TYPE_NEGOTIATE    = 0xA
        const val TYPE_NEGOTIATE_ACK    = 0xB
        const val TYPE_PROVISION_REQ    = 0xC
        const val TYPE_COMMAND    = 0xD

        val PacketMapping = mutableMapOf(
            TYPE_AUTH            to { Auth()              },
            TYPE_AUTHRES         to { AuthResponse()      },
            TYPE_CONFIG          to { Config()            },
            TYPE_SYNCHRONIZE     to { Synchronize()       },
            TYPE_HEARTBEAT       to { Heartbeat()         },
            TYPE_INPUT           to { Input()             },
            TYPE_OUTPUT          to { Output()            },
            TYPE_EVENT           to { Event()             },
            TYPE_PROVISION       to { Provision()         },
            TYPE_PROVISIONRES    to { ProvisionResponse() },
            TYPE_NEGOTIATE       to { Negotiate()         },
            TYPE_NEGOTIATE_ACK   to { NegotiateAck()      },
            TYPE_PROVISION_REQ   to { ProvisionRequest()  },
            TYPE_COMMAND         to { Command()           }
        )

    }

}