package team.aries.helios

import java.awt.Color

fun String.color256(color: Int): String {
    return "\u001b[38;5;${color}m${this}"
}

fun String.color16bit(color: Color): String {
    return "\u001b[38;2;${color.red};${color.green};${color.blue}m${this}"
}

fun String.reset(): String {
    return "${this}\u001b[0m"
}

fun String.bold(): String {
    return "\u001b[1m${this}"
}

fun clearScreen(): String {
    return "\u001b[2J"
}