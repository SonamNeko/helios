package team.aries.helios.api

data class Package
(
    val id: String = "unknown",
    val versions: MutableList<String> = mutableListOf("0.1"),
    val latest: String = "0.1",
    val variables: MutableList<String> = mutableListOf(),
    val preInitScript: MutableList<String> = mutableListOf(),
    val depends: MutableList<String> = mutableListOf(),
    val parseFiles: MutableList<String> = mutableListOf(),
    val entryPoint: String? = null

)