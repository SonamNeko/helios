package team.aries.helios.api

import com.google.gson.Gson
import io.netty.buffer.ByteBuf

interface HeliosPacket {

    companion object {
        val Mapper = Gson()
    }

    fun read(buffer: ByteBuf)

    fun write(buffer: ByteBuf)

    fun handle(handler: AbstractPacketHandler)

    val id: Int

}