package team.aries.helios.api

import team.aries.helios.packet.*
import team.aries.helios.packet.coordbound.AuthResponse
import team.aries.helios.packet.coordbound.Command
import team.aries.helios.packet.coordbound.Provision
import team.aries.helios.packet.coordbound.ProvisionRequest
import team.aries.helios.packet.serverbound.*

abstract class AbstractPacketHandler {

    open fun handle(input: Input) {}

    open fun handle(output: Output) {}

    open fun handle(event: Event) {}

    open fun handle(synchronize: Synchronize) {}

    open fun handle(heartbeat: Heartbeat) {}

    open fun handle(config: Config) {}

    open fun handle(auth: Auth) {}

    open fun handle(provision: Provision) {}

    open fun handle(provisionRes: ProvisionResponse) {}

    open fun handle(authRes: AuthResponse) {}

    open fun handle(negotiate: Negotiate) {}

    open fun handle(ack: NegotiateAck) {}

    open fun handle(req: ProvisionRequest) {}

    open fun handle(command: Command) {}

    abstract fun onConnected()

    abstract fun onDisconnected()


}