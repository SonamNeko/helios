package team.aries.helios.api

import java.util.*

data class CoordProcess
(
    val uuid: UUID,
    val name: String,
    val start: Long,
    val rootPackage: String,
    val status: Status
) {
    enum class Status {
        RUNNING, STOPPING, PROVISIONING, STARTING, NOT_RUNNING
    }
}