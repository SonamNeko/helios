package team.aries.helios

import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelPipeline
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.Delimiters
import io.netty.handler.codec.LengthFieldBasedFrameDecoder
import io.netty.handler.codec.LengthFieldPrepender
import io.netty.handler.codec.compression.ZlibCodecFactory
import io.netty.handler.codec.compression.ZlibWrapper
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import team.aries.helios.netty.AESDecoder
import team.aries.helios.netty.AESEncoder
import team.aries.helios.netty.HeliosDecoder
import team.aries.helios.netty.HeliosEncoder
import javax.crypto.SecretKey

object PipelineUtils {

    private fun getNewFrameDecoder() = LengthFieldBasedFrameDecoder(Int.MAX_VALUE, 0, 4, 0, 4)
    private fun getNewFramePrepender() = LengthFieldPrepender(4)

    private const val FRAME_DECODER_KEY = "frame_decoder"
    private const val FRAME_PREPENDER_KEY = "frame_prepender"
    private const val HELIOS_DECODER_KEY = "helios_decoder"
    private const val HELIOS_ENCODER_KEY = "helios_encoder"
    private const val ZLIB_ENCODER = "zlib_encoder"
    private const val ZLIB_DECODER = "zlib_decoder"
    private const val AES_ENCODER = "aes_encoder"
    private const val AES_DECODER = "aes_decoder"

    fun setupEncryption(pipeline: ChannelPipeline, secretKey: SecretKey) {
        //To make sure we don't have duplicates
        pipeline.addAfter(FRAME_PREPENDER_KEY, AES_DECODER, AESDecoder(secretKey))
        pipeline.addAfter(AES_DECODER, AES_ENCODER, AESEncoder(secretKey))
    }

    fun setupPipeline(pipeline: ChannelPipeline, vararg logicHandlers: ChannelHandler) {
        pipeline.addLast(ZLIB_DECODER, ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP))
        pipeline.addLast(ZLIB_ENCODER, ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP, 6))
        pipeline.addLast(FRAME_DECODER_KEY, getNewFrameDecoder())
        pipeline.addLast(FRAME_PREPENDER_KEY, getNewFramePrepender())
        pipeline.addLast(HELIOS_DECODER_KEY, HeliosDecoder())
        pipeline.addLast(HELIOS_ENCODER_KEY, HeliosEncoder())
        for (logicHandler in logicHandlers) {
            pipeline.addLast(logicHandler)
        }
    }

}