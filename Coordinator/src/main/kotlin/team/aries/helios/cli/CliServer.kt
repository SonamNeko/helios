package team.aries.helios.cli

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInitializer
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollServerSocketChannel
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.*
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import team.aries.helios.Coordinator
import team.aries.helios.netty.AESDecoder
import team.aries.helios.netty.AESEncoder
import team.aries.helios.provision.ProvisionedProcess
import java.net.ServerSocket

class CliServer(private val host: String) {

    val port = getFreePort()

    fun start() {

        Runtime.getRuntime().addShutdownHook(Thread {
            Coordinator.get().provisionManager.processes.filter { it is ProvisionedProcess }.forEach {
                it.end()
            }
        })

        Coordinator.get().logger.info("Attempting to listen on [$host:$port]")
        ServerBootstrap()
            .channel(EpollServerSocketChannel::class.java)
            .group(EpollEventLoopGroup(), EpollEventLoopGroup())
            .childHandler(object: ChannelInitializer<SocketChannel>() {
                override fun initChannel(ch: SocketChannel) {
                    ch.pipeline()
                        .addLast(LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4))
                        .addLast(LengthFieldPrepender(4))
                        .addLast(AESDecoder(Coordinator.get().key))
                        .addLast(AESEncoder(Coordinator.get().key))
                        .addLast(DelimiterBasedFrameDecoder(2048, *Delimiters.nulDelimiter()))
                        .addLast(StringDecoder())
                        .addLast(StringEncoder())
                        .addLast(object: SimpleChannelInboundHandler<String>() {

                            private lateinit var ctx: ChannelHandlerContext
                            private lateinit var cliHandler: CliCommandHandler
                            override fun channelActive(ctx: ChannelHandlerContext) {
                                this.ctx = ctx
                                cliHandler = CliCommandHandler(ctx.channel())
                                Coordinator.get().logger.info("Passthrough connected <${ctx.channel().remoteAddress()}>")
                            }

                            override fun channelRead0(ctx: ChannelHandlerContext, str: String) {
                                cliHandler.handle(str)
                            }

                            override fun channelInactive(ctx: ChannelHandlerContext) {
                                Coordinator.get().logger.info("Passthrough disconnected <${ctx.channel().remoteAddress()}>")
                            }
                        })
                }
            }).bind(host, port).addListener {
                if(it.isSuccess) {
                    Coordinator.get().logger.info("Listening on on [$host:$port]")
                }
            }
    }

    private fun getFreePort(): Int {
        val socket = ServerSocket(0)
        return socket.run {
            this.close()
            this.localPort
        }
    }
}