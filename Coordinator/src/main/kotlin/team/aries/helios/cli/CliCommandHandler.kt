package team.aries.helios.cli

import io.netty.channel.Channel
import team.aries.helios.Coordinator
import team.aries.helios.clearScreen
import team.aries.helios.packet.coordbound.Provision
import team.aries.helios.provision.CProcess
import team.aries.helios.provision.ProvisionedProcess
import java.lang.Exception
import java.util.*


class CliCommandHandler(val channel: Channel) {

    private var attachedProcess: CProcess? = null

    fun handle(input: String) {
        val split = input.split(" ")
        val cmd = split[0].toLowerCase()
        val args = if(split.size == 1) emptyList() else split.drop(1)

        if(attachedProcess != null) {
            if(cmd == "d") {
                attachedProcess?.detach(this@CliCommandHandler)
                write(clearScreen())
                writeLine("Detached from ${attachedProcess!!.name}:${attachedProcess!!.uuid}")
                attachedProcess = null
            } else {
                attachedProcess!!.sendToInput(input)
            }
            return
        }

        Coordinator.get().logger.info("Passthrough <${channel.remoteAddress()}> issued command: $input")

        if(cmd == "clear") {
            writeLine(clearScreen())
            return
        }

        if(cmd == "kill") {
            if(args.isEmpty()) {
                writeLine("Provide a process number, uuid, or name")
                return
            }
            val process = if(args[0].toIntOrNull() != null) {
                Coordinator.get().provisionManager.processes.elementAt(args[0].toInt())
            } else {
                Coordinator.get().provisionManager.processes.find { try {it.uuid == UUID.fromString(args[0]) } catch(ex: Exception) { it.name == args[0] } }
            }
            process?.connection?.sendSignal("KILL")
        }

        if(cmd == "provision") {
            if(args.size < 2) {
                writeLine("Provide a root package name and process name")
                return
            }
            writeLine("Provisioning process \'${args[0]}\' with root package \'${args[1].toLowerCase()}\'")

            val vars = mutableMapOf<String, String>()

            if(args.indexOf("-v") != -1) {
                val index = args.indexOf("-v")
                if(args.size == (index - 1)) {
                    writeLine("-v must be followed by a variable assignment string")
                    return
                }
                val str = args[index + 1].split(";")
                str.forEach {
                    val finalVar = it.split("=")
                    if(finalVar.size == 2) {
                        vars[finalVar[0]] = finalVar[1]
                    }
                }
            }

            println(vars)

            Coordinator.get().provisionManager.provision(Provision(args[0], args[1].toLowerCase(), listOf(), vars)) {
                writeLine("Started provisioning ${args[0]} : $it")
            }
            return
        }

        if(cmd == "list") {
            Coordinator.get().provisionManager.processes.forEachIndexed { index, it ->
                writeLine("$index: ${it.uuid} : ${it.name}")
            }
            return
        }

        if(cmd == "attach") {
            if(args.isEmpty()) {
                writeLine("Provide a process number, uuid, or name")
                return
            }
            val process = if(args[0].toIntOrNull() != null) {
                Coordinator.get().provisionManager.processes.elementAt(args[0].toInt())
            } else {
                Coordinator.get().provisionManager.processes.find { try {it.uuid == UUID.fromString(args[0]) } catch(ex: Exception) { it.name == args[0] } }
            }

            process?.let {
                writeLine("Attaching to process ${it.name}:${it.uuid}\n\n")
                it.attach(this@CliCommandHandler)
                attachedProcess = it
            }
            return
        }

        if(cmd == "restart") {
            if(args.isEmpty()) {
                writeLine("Provide a process number, uuid, or name")
                return
            }
            val process = if(args[0].toIntOrNull() != null) {
                Coordinator.get().provisionManager.processes.elementAt(args[0].toInt())
            } else {
                Coordinator.get().provisionManager.processes.find { try {it.uuid == UUID.fromString(args[0]) } catch(ex: Exception) { it.name == args[0] } }
            }

            process?.let {
                it.connection?.sendSignal("RESTART")
            }
        }

    }

    fun onDetach(process: CProcess) {
        attachedProcess = null
    }

    fun write(string: String) {
        channel.writeAndFlush(string)
    }
    fun writeLine(string: String) {
        channel.writeAndFlush("$string\n\r\u0000")
    }

}