package team.aries.helios.handler

import team.aries.helios.Coordinator
import team.aries.helios.PipelineUtils
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.CoordConnection
import team.aries.helios.netty.AESDecoder
import team.aries.helios.netty.AESEncoder
import team.aries.helios.packet.NegotiateAck
import team.aries.helios.packet.coordbound.AuthResponse
import team.aries.helios.packet.coordbound.Command
import team.aries.helios.packet.coordbound.Provision
import team.aries.helios.packet.coordbound.ProvisionRequest
import team.aries.helios.packet.serverbound.Auth
import team.aries.helios.packet.serverbound.Config
import team.aries.helios.packet.serverbound.ProvisionResponse
import team.aries.helios.packet.serverbound.Synchronize
import java.util.*
import java.util.concurrent.TimeUnit

class CoordInboundHandler(private val coordConnection: CoordConnection) : AbstractPacketHandler() {

    override fun handle(authRes: AuthResponse) {
        if(authRes.success) {
            Coordinator.get().logger.info("Authenticated!")
            Coordinator.get().logger.info("Registering CLI passthrough endpoint on Helios")
            Coordinator.get().scheduler.scheduleAtFixedRate({
                Coordinator.get().provisionManager.processes
                    .map { it.asCoordProcess() }
                    .toList().let {
                        coordConnection.write(Synchronize(it))
                    }
            }, 1000L, 1000L, TimeUnit.MILLISECONDS)
            Coordinator.get().heliosConfig = authRes.config
            coordConnection.write(Config().apply {
                hostTunnel = Coordinator.get().config.cliPassthroughHost
                portTunnel = Coordinator.get().cliServer.port
            })
        }
    }

    override fun handle(ack: NegotiateAck) {
        if(ack.bound) {
            Coordinator.get().logger.info("AES Encryption Key Received")
            val key = ack.decryptKey(Coordinator.get().keyPair.private)
            Coordinator.get().key = key
            coordConnection.key = key
            PipelineUtils.setupEncryption(coordConnection.ctx.channel().pipeline(), key)
            Coordinator.get().logger.info("Authenticating...")
            coordConnection.write(Auth(Coordinator.get().config.key.toByteArray(), "Coord:${UUID.randomUUID()}"))
        } else {
            Coordinator.get().logger.fatal("Encryption Negotiation failed")
        }
    }

    override fun handle(provision: Provision) {
        Coordinator.get().provisionManager.provision(provision)
    }

    override fun handle(req: ProvisionRequest) {
        Coordinator.get().provisionManager.provision(req.provision) {
            coordConnection.write(ProvisionResponse(req.ackId, req.provision.serverName, it))
        }
    }

    override fun handle(command: Command) {
        when(command.label.toUpperCase()) {
            "STOP" -> {
                Coordinator.get().provisionManager.processes.asSequence().filter { it.uuid.toString() == command.args[0] }
                    .first().connection?.sendEndSignal()
            }
            "START" -> {
                Coordinator.get().provisionManager.processes.asSequence().filter { it.uuid.toString() == command.args[0] }
                    .first().connection?.sendRestartSignal()
            }
        }
    }

    override fun onConnected() {

    }

    override fun onDisconnected() {

    }
}