package team.aries.helios

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import io.netty.bootstrap.Bootstrap
import io.netty.channel.Channel
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelOption
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollSocketChannel
import io.netty.channel.socket.SocketChannel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import team.aries.helios.api.Configuration
import team.aries.helios.api.CoordConnection
import team.aries.helios.api.HeliosPacket
import team.aries.helios.cli.CliServer
import team.aries.helios.packet.Output
import team.aries.helios.provision.ProvisionManager
import team.aries.helios.uds.ProcessUDSServer
import java.awt.Color
import java.io.File
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.crypto.SecretKey

class Coordinator {

    companion object {
        private lateinit var instance: Coordinator
        fun get() = instance
    }

    lateinit var config: Configuration
    lateinit var keyPair: KeyPair
    lateinit var cliServer: CliServer
    private lateinit var bootstrap: Bootstrap
    lateinit var key: SecretKey
    val scheduler = Executors.newScheduledThreadPool(2)
    val logger = LogManager.getLogger(this::class.java)
    val provisionManager = ProvisionManager(this)
    private var channel: Channel? = null
    lateinit var heliosConfig: JsonObject
    lateinit var processComms: ProcessUDSServer

    init {
        instance = this
    }

    fun start() {
        println("""
            *----------------------------------------------------
            *
            *    |    |  |‾‾‾‾‾  |       |   |‾‾‾‾‾|   |‾‾‾‾‾
            *    |————|  |———    |       |   |     |   |_____
            *    |    |  |_____  |_____  |   |_____|    _____|
            *
            *                   Coordinator
            *                  Aries Network
            *                   Version 1.1
            *
            *----------------------------------------------------
        """.trimMargin("*").color16bit(Color(239,222,153)).bold().reset())
        initialize()
    }

    private fun generateRsaKeyPair() {
        logger.info("Generating RSA Key Pair...")
        val generator = KeyPairGenerator.getInstance("RSA")
        generator.initialize(2048)
        keyPair = generator.genKeyPair()
        logger.info("RSA Key Pair Generated")
    }

    private fun loadConfiguration() {
        val file = File("config.json")
        val gson = GsonBuilder().setPrettyPrinting().create()
        config = if(file.exists()) {
            gson.fromJson(file.readText())
        } else {
            Configuration().apply {
                file.writeText(gson.toJson(this@apply))
            }
        }
    }

    private fun initialize() {
        generateRsaKeyPair()
        loadConfiguration()
        startCliPassthroughServer()
        startUDSCommunication()
        connectToHelios()
    }

    private fun startUDSCommunication() {
        logger.info("UDS Inter-process communication starting at <coordinator.sock>")
        GlobalScope.launch {
            processComms = ProcessUDSServer()
            processComms.start()
        }
    }

    fun connectToHelios() {
        logger.info("Attempting connection with Helios [${config.host}:${config.port}]")
        bootstrap = Bootstrap()
            .channel(EpollSocketChannel::class.java)
            .group(EpollEventLoopGroup())
            .option(ChannelOption.TCP_NODELAY, true)
            .option(ChannelOption.AUTO_READ, true)
            .handler(object: ChannelInitializer<SocketChannel>() {
                override fun initChannel(channel: SocketChannel) {
                    this@Coordinator.channel = channel
                    channel.pipeline().apply {
                        PipelineUtils.setupPipeline(this, CoordConnection())
                    }
                }
            }).apply {
                connect(config.host, config.port)
                .addListener {
                    if(it.isSuccess) {
                        logger.info("Connected to Helios [${config.host}:${config.port}]")
                    } else {
                        logger.fatal("Could not connect to Helios [${config.host}:${config.port}], reattempting in ${config.autoReconnectInterval / 1000} seconds")
                        scheduler.schedule({ connectToHelios() }, config.autoReconnectInterval, TimeUnit.MILLISECONDS)
                    }
                }
            }
    }

    fun duplexLog(uuid: UUID, string: String) {
        logger.info("[$uuid] $string")
        this.channel?.writeAndFlush(Output(string))
    }

    fun writeToHelios(heliosPacket: HeliosPacket) {
        this.channel?.writeAndFlush(heliosPacket)
    }

    private fun startCliPassthroughServer() {
        logger.info("Starting CLI Passthrough Server...")
        cliServer = CliServer(config.cliPassthroughHost)
        cliServer.start()
    }

}