package team.aries.helios.uds

import io.netty.channel.Channel
import team.aries.helios.provision.CProcess

class UDSConnection(private val channel: Channel) {

    private var outputCallback: ((String) -> Unit)? = null
    var process: CProcess? = null

    fun onOutput(string: String) {
        outputCallback?.invoke(string)
    }

    fun attachOutput(callback: (String) -> Unit) {
        this.outputCallback = callback
    }

    fun sendSignal(signal: String) {
        channel.writeAndFlush("$signal\u0000")
    }

    fun sendStartSignal() {
        channel.writeAndFlush("START\u0000")
    }

    fun sendRestartSignal() {
        channel.writeAndFlush("RESTART\u0000")
    }

    fun sendEndSignal() {
        channel.writeAndFlush("END\u0000")
    }

    fun sendInput(string: String) {
        channel.writeAndFlush("IN $string\u0000")
    }

}