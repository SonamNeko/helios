package team.aries.helios.uds

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.*
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollServerDomainSocketChannel
import io.netty.channel.unix.DomainSocketAddress
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.Delimiters
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import team.aries.helios.Coordinator
import team.aries.helios.provision.ExternalProcess
import team.aries.helios.provision.ProvisionedProcess
import java.util.*

class ProcessUDSServer {

    fun start() {
        ServerBootstrap()
            .channel(EpollServerDomainSocketChannel::class.java)
            .group(EpollEventLoopGroup())
            .childOption(ChannelOption.AUTO_READ, true)
            .childHandler(object: ChannelInitializer<Channel>() {
                override fun initChannel(chn: Channel) {
                    chn.pipeline().apply {
                        addLast(DelimiterBasedFrameDecoder(1024, *Delimiters.nulDelimiter()))
                        addLast(StringDecoder())
                        addLast(StringEncoder())
                        addLast(object: SimpleChannelInboundHandler<String>() {

                            private lateinit var udsConnection: UDSConnection

                            override fun channelActive(ctx: ChannelHandlerContext) {
                                this.udsConnection = UDSConnection(ctx.channel())
                            }

                            override fun channelRead0(ctx: ChannelHandlerContext, str: String) {
                                if(str.startsWith("EXTERNAL")) {
                                    val name = str.split(" ")[1]
                                    Coordinator.get().provisionManager.processes.add(ExternalProcess(name, udsConnection).apply {
                                        udsConnection.process = this
                                        this.start()
                                    })
                                }
                                if(str.startsWith("CONNECT")) {
                                    val pid = str.split(" ")[1]
                                    Coordinator.get().provisionManager.processes
                                        .find { it.uuid == UUID.fromString(pid) && it is ProvisionedProcess }
                                        ?.apply {
                                            this as ProvisionedProcess
                                            this.onUdsConnect(udsConnection)
                                            udsConnection.process = this
                                        }
                                }
                                if(str == "STARTING") {
                                    udsConnection.process?.onStartingSignal()
                                }
                                if(str == "NOT_RUNNING") {
                                    if(udsConnection.process is ExternalProcess) {
                                        (udsConnection.process as ExternalProcess).onStopNonEndSignal()
                                    }
                                }
                                if(str == "RUNNING") {
                                    if(udsConnection.process is ExternalProcess) {
                                        (udsConnection.process as ExternalProcess).onRunningSignal()
                                    }
                                }
                                if(str.startsWith("OUT ")) {
                                    udsConnection.onOutput(str.drop(4))
                                }
                            }

                            override fun channelInactive(ctx: ChannelHandlerContext) {
                                udsConnection.process?.onEndingSignal()
                            }
                        })
                    }
                }
            })
            .bind(DomainSocketAddress("coordinator.sock"))
            .addListener {
                if(it.isSuccess) {
                    Coordinator.get().logger.info("UDS Inter-process communication running")
                }
            }.channel().closeFuture().sync()
    }

}