package team.aries.helios.provision

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import team.aries.helios.Coordinator
import team.aries.helios.api.CoordProcess
import team.aries.helios.api.Package
import team.aries.helios.cli.CliCommandHandler
import team.aries.helios.packet.coordbound.Provision
import team.aries.helios.uds.UDSConnection
import java.io.File
import java.net.ServerSocket
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

class ProvisionedProcess(private val provision: Provision, private val coordinator: Coordinator, val onDispose: ProvisionedProcess.() -> Unit)
    : CProcess {

    override var connection: UDSConnection? = null

    data class PackageVariable
    (
        val name: String,
        val type: String,
        val default: String? = null
    ) {

        fun generateValue(): String {
            if(type == "host")
                return "0.0.0.0"
            if(type == "port")
                return "${getFreePort()}"
            return ""
        }

        private fun getFreePort(): Int {
            val socket = ServerSocket(0)
            return socket.run {
                this.close()
                this.localPort
            }
        }
    }

    override var status: CoordProcess.Status = CoordProcess.Status.NOT_RUNNING

    private val client = OkHttpClient()
    private val mapper = Gson()
    override val start = System.currentTimeMillis()

    private val packages = mutableMapOf<String, Package>()
    private val logs = mutableListOf<String>()

    private val providerPort = coordinator.heliosConfig.get("providerPort").asInt
    private val providerHost = coordinator.heliosConfig.get("providerHost").asString

    private val attached = ConcurrentLinkedQueue<CliCommandHandler>()
    private val variables = mutableListOf<PackageVariable>()
    private var ending = false
    private val daemonName = "Daemon.jar"

    private var delegateProcess: Process? = null

    override val uuid = UUID.randomUUID()!!
    override val name = provision.serverName
    private val dataDir = File("$uuid")

    override fun start() {

        val authKey = String(Base64.getEncoder().encode(Coordinator.get().key.encoded))

        this.status = CoordProcess.Status.PROVISIONING

        coordinator.duplexLog(uuid,"Starting provision of delegateProcess \'${provision.serverName}\' : $uuid : using \'${provision.`package`}\' as root package")

        dataDir.mkdir()
        File(dataDir, "packages/").mkdir()

        val manifest = Request.Builder()
            .url("http://$providerHost:$providerPort/manifest/${provision.`package`}")
            .header("Auth-Key", authKey)
            .build().run {
                client.newCall(this).execute().body()?.string()
            }

        if(manifest == null) {
            coordinator.duplexLog(uuid,"Process '${provision.serverName}' : $uuid failed to provision, could not fetch manifest")
            end()
            return
        }

        val pkgManifest = mapper.fromJson<Package>(manifest)

        packages[pkgManifest.id] = pkgManifest

        if(pkgManifest.entryPoint == null) {
            coordinator.duplexLog(uuid,"Process '${provision.serverName}' : $uuid failed to provision, root package does not have entry point")
            end()
            return
        }

        coordinator.logger.info("[$uuid] Retrieved root package manifest $pkgManifest")

        pkgManifest.depends.forEach {
            recurseManifest(it)
        }

        packages.forEach {
            coordinator.logger.info("[$uuid] Retrieving package \'${it.key}\'")
            ProcessBuilder(*"""curl --header Auth-Key:$authKey http://$providerHost:$providerPort/package/${it.key}/latest --output ./$uuid/packages/${it.key}.zip""".split(" ").toTypedArray())
                .start().waitFor()
        }

        coordinator.logger.info("[$uuid] Retrieving process delegate daemon client")
        ProcessBuilder(*"""curl --header Auth-Key:$authKey http://$providerHost:$providerPort/daemon --output ./$uuid/$daemonName""".split(" ").toTypedArray())
            .start().waitFor()

        val zipPackages = File("$uuid/packages/")

        zipPackages.listFiles().forEach {
            if(it.extension == "zip") {
                ProcessBuilder(*"unzip ./$uuid/packages/${it.name} -d ./$uuid/".split(" ").toTypedArray())
                    .start().waitFor()
            }
        }

        packages.forEach {
            it.value.variables.forEach { input ->
            val split = input.split(":")
            val varName = split[0]
            val varType = split[1]
            val varDefault = if(split.size < 3) null else split[2]
            variables.add(PackageVariable(varName, varType, varDefault))
        }
        }

        val filesToParse = mutableListOf<String>()

        packages.filter { it.value.parseFiles.isNotEmpty() }
            .forEach {
                filesToParse.addAll(it.value.parseFiles)
            }

        val entryPoint = parseVariables(findEntryPoint(pkgManifest))

        dataDir.listFiles().forEach {
            if(filesToParse.contains(it.name)) {
                val newText = parseVariables(it.readText())
                it.writeText(newText)
            }
        }

        val formatEntryPoint = entryPoint
            .replace("\$ENV", provision.environment.joinToString(" ").trim())
            .replace("\$PROCESS_DIR", "./$uuid")
            .replace("\$PROCESS_NAME", name)

        coordinator.logger.info("Starting at entry point: $formatEntryPoint")

        File("./$uuid/.entrypoint").writeText(formatEntryPoint.trim())

        delegateProcess = ProcessBuilder(*"java -jar $daemonName -pid $uuid -socket ../coordinator.sock".split(" ").toTypedArray())
            .directory(dataDir)
//            .inheritIO()
            .redirectError(ProcessBuilder.Redirect.PIPE)
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectInput(ProcessBuilder.Redirect.PIPE)
            .start()
    }

    fun onUdsConnect(udsConnection: UDSConnection) {
        this.connection = udsConnection
        this.status = CoordProcess.Status.STARTING
        udsConnection.sendStartSignal()
        udsConnection.attachOutput {
            if(logs.size == 50) {
                logs.removeAt(0)
            }
            logs.add(it)
            attached.forEach { handler ->
                handler.writeLine(it)
            }
        }
    }

    override fun onStartingSignal() {
        this.status = CoordProcess.Status.RUNNING
        coordinator.duplexLog(uuid,"Process '${provision.serverName}' : $uuid provisioned and running")
    }

    override fun onEndingSignal() {
        this.status = CoordProcess.Status.STOPPING
        attached.forEach {
            it.writeLine("Detached, process ended")
            it.onDetach(this)
        }
        attached.clear()
        Coordinator.get().duplexLog(uuid,"Process '$name' : $uuid ended")
        end()
    }

    private fun parseVariables(input: String): String {
        var new = input
        variables.forEach {
            if(new.contains("\$${it.name}")) {
                new = if(provision.variables.containsKey(it.name)) {
                    new.replace("\$${it.name}", provision.variables.getValue(it.name))
                } else {
                    if(it.default != null) {
                        new.replace("\$${it.name}", it.default)
                    } else {
                        new.replace("\$${it.name}", it.generateValue())
                    }
                }
            }
        }
        return new
    }

    override fun end() {
        if(ending) return
        ending = true
        if(delegateProcess?.isAlive == true) {
            connection?.sendSignal("KILL")
        }
        dispose()
    }



    private fun findEntryPoint(pkg: Package): String {
        if(pkg.entryPoint!!.startsWith("inherit:")) {
            val inheritingPackage = pkg.entryPoint!!.split(":")[1]
            if(!packages.containsKey(inheritingPackage)) {
                coordinator.duplexLog(uuid,"Failed to provision, package inherits entry point, but doesn't depend on it")
                end()
                return ""
            } else {
                val inheriting = packages[inheritingPackage]!!
                return if(inheriting.entryPoint == null) {
                    coordinator.duplexLog(uuid,"Failed to provision, package inherits entry point, but the inherited package doesn't have an entry point")
                    end()
                    ""
                } else {
                    if(inheriting.entryPoint!!.startsWith("inherit:")) {
                        findEntryPoint(inheriting)
                    } else {
                        inheriting.entryPoint!!
                    }
                }
            }
        } else {
            return pkg.entryPoint!!
        }
    }

    override fun attach(channel: CliCommandHandler) {
        logs.forEach {
            channel.writeLine(it)
        }
        attached.add(channel)
    }

    override fun detach(channel: CliCommandHandler) {
        attached.remove(channel)
    }

    override fun sendToInput(string: String) {
        connection?.sendInput(string)
    }

    override fun asCoordProcess(): CoordProcess {
        return CoordProcess(this.uuid, this.name, this.start, this.provision.`package`, status)
    }

    private fun recurseManifest(pkg: String) {
        val manifest = Request.Builder()
            .url("http://$providerHost:$providerPort/manifest/$pkg")
            .build().run {
                client.newCall(this).execute().body()?.string()
            }

        if(manifest == null) {
            coordinator.duplexLog(uuid,"Process \'$uuid\' failed to provision, could not fetch child manifest \'$pkg\'")
            end()
            return
        }
        val pkgManifest = mapper.fromJson<Package>(manifest)
        if(!packages.containsKey(pkg)) {
            packages[pkg] = pkgManifest
        }
        coordinator.logger.info("[$uuid] Retrieved child package manifest $pkgManifest")
        pkgManifest.depends.forEach {
            recurseManifest(it)
        }
    }

    fun uptime() = System.currentTimeMillis() - start

    private fun dispose() {
        dataDir.deleteRecursively()
        this.onDispose()
        coordinator.duplexLog(uuid,"Process has ended and been disposed")
    }

    override fun equals(other: Any?): Boolean {
        return if(other is ProvisionedProcess) {
            other.uuid == this.uuid
        } else {
            false
        }
    }
}