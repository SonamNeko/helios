package team.aries.helios.provision

import team.aries.helios.Coordinator
import team.aries.helios.api.CoordProcess
import team.aries.helios.cli.CliCommandHandler
import team.aries.helios.uds.UDSConnection
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

class ExternalProcess(override val name: String, override var connection: UDSConnection?) : CProcess {

    override var status: CoordProcess.Status = CoordProcess.Status.NOT_RUNNING
    override val uuid: UUID = UUID.randomUUID()
    private val attached = ConcurrentLinkedQueue<CliCommandHandler>()
    private val logs = mutableListOf<String>()
    override val start: Long = System.currentTimeMillis()

    override fun start() {
        this.status = CoordProcess.Status.STARTING
        connection?.sendStartSignal()
        Coordinator.get().duplexLog(uuid,"External process '$name' : $uuid connected")
        this.connection?.attachOutput {
            if(logs.size == 50) {
                logs.removeAt(0)
            }
            logs.add(it)
            attached.forEach { handler ->
                handler.writeLine(it)
            }
        }
    }

    fun sendRestartSignal() {
        connection?.sendSignal("RESTART")
    }

    fun onStopNonEndSignal() {
        this.status = CoordProcess.Status.NOT_RUNNING
        attached.forEach {
            it.writeLine("Process stopped, but is persistent. If no restart flag is found in process, manual restart is required.\nDetached")
            it.onDetach(this)
        }
    }

    fun onRunningSignal() {
        this.status = CoordProcess.Status.RUNNING
    }

    override fun onEndingSignal() {
        this.status = CoordProcess.Status.NOT_RUNNING
        attached.forEach {
            it.writeLine("Detached, process ended")
            it.onDetach(this)
        }
        attached.clear()
        Coordinator.get().duplexLog(uuid,"External process '$name' : $uuid disconnected")
        Coordinator.get().provisionManager.processes.remove(this)
    }

    override fun attach(channel: CliCommandHandler) {
        logs.forEach {
            channel.writeLine(it)
        }
        attached.add(channel)
    }

    override fun detach(channel: CliCommandHandler) {
        attached.remove(channel)
    }

    override fun onStartingSignal() {
        this.status = CoordProcess.Status.STARTING
    }

    override fun sendToInput(string: String) {
        connection?.sendInput(string)
    }

    override fun end() {
        connection?.sendEndSignal()
    }
}