package team.aries.helios.provision

import java.io.BufferedReader
import java.io.Reader

class CloseAwareReader(reader: Reader) : BufferedReader(reader) {

    var isClosed = false

    override fun close() {
        isClosed = true
        super.close()
    }
}