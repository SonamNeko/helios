package team.aries.helios.provision

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import team.aries.helios.Coordinator
import team.aries.helios.packet.coordbound.Provision
import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque

class ProvisionManager(private val coordinator: Coordinator) {

    val processes = ConcurrentLinkedDeque<CProcess>()

    fun provision(provision: Provision, callback: (UUID) -> Unit = {}) {
        GlobalScope.launch {
            ProvisionedProcess(provision, coordinator) { processes.remove(this) }.let {
                processes.add(it)
                callback(it.uuid)
                it.start()
            }
        }
    }



}