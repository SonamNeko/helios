package team.aries.helios.provision

import team.aries.helios.api.CoordProcess
import team.aries.helios.cli.CliCommandHandler
import team.aries.helios.uds.UDSConnection
import java.util.*

interface CProcess {

    var connection: UDSConnection?

    var status: CoordProcess.Status

    val uuid: UUID

    val name: String

    val start: Long

    fun start()

    fun attach(channel: CliCommandHandler)

    fun detach(channel: CliCommandHandler)

    fun sendToInput(string: String)

    fun onStartingSignal()

    fun onEndingSignal()

    fun end() {}

    fun asCoordProcess(): CoordProcess { return CoordProcess(uuid, name, start, "external", status) }

}