package team.aries.helios.api

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import team.aries.helios.Coordinator
import team.aries.helios.handler.CoordInboundHandler
import team.aries.helios.packet.Negotiate
import team.aries.helios.packet.serverbound.Auth
import java.security.PublicKey
import java.util.concurrent.TimeUnit
import javax.crypto.SecretKey

class CoordConnection : SimpleChannelInboundHandler<HeliosPacket>() {

    lateinit var ctx: ChannelHandlerContext
    var key: SecretKey? = null
    private val handler = CoordInboundHandler(this)

    override fun channelInactive(ctx: ChannelHandlerContext) {
        Coordinator.get().logger.info("Lost connection with Helios")
        if(Coordinator.get().config.autoReconnect) {
            Coordinator.get().logger.info("Reattempting connection with Helios in ${Coordinator.get().config.autoReconnectInterval / 1000} seconds")
            Coordinator.get().scheduler.schedule({
                Coordinator.get().connectToHelios()
            }, Coordinator.get().config.autoReconnectInterval, TimeUnit.MILLISECONDS)
        }
    }

    override fun channelActive(ctx: ChannelHandlerContext) {
        this.ctx = ctx
        Coordinator.get().logger.info("Negotiating encryption...")
        write(Negotiate(Coordinator.get().keyPair.public))
    }

    override fun channelRead0(ctx: ChannelHandlerContext, packet: HeliosPacket) {
        packet.handle(handler)
    }

    fun write(heliosPacket: HeliosPacket) = ctx.writeAndFlush(heliosPacket)

}