package team.aries.helios.api

data class Configuration
(
    val host: String = System.getProperty("helios.host", "localhost"),
    val port: Int = System.getProperty("helios.port", "4950").toIntOrNull() ?: 4950,
    val key: String = "123456789",
    val cliPassthroughHost: String = System.getProperty("helios.cli", "localhost"),
    val autoReconnect: Boolean = true,
    val autoReconnectInterval: Long = 5000
)