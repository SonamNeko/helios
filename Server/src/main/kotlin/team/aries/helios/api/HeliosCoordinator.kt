package team.aries.helios.api

import io.netty.channel.ChannelDuplexHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import team.aries.helios.Helios
import team.aries.helios.handler.NotAuthInboundHandler
import team.aries.helios.packet.serverbound.Config
import java.net.InetSocketAddress
import java.security.PublicKey
import java.util.*
import javax.crypto.KeyGenerator

class HeliosCoordinator : SimpleChannelInboundHandler<HeliosPacket>() {

    private var handler: AbstractPacketHandler = NotAuthInboundHandler(this)
    lateinit var ctx: ChannelHandlerContext
    var name: String = "_UNKNOWN_"
    var authenticated = false
    var publicKey: PublicKey? = null
    val processes = mutableListOf<CoordProcess>()
    var configuration: Config? = null
    val encryptionKey = KeyGenerator.getInstance("AES").run {
        this.init(256)
        this.generateKey()
    }

    fun ipAddress() = (ctx.channel().remoteAddress() as InetSocketAddress).hostName

    override fun channelInactive(ctx: ChannelHandlerContext) {
        Helios.connectionManager.removeConnection(this)
        handler.onDisconnected()
    }

    override fun channelActive(ctx: ChannelHandlerContext) {
        this.ctx = ctx
        Helios.connectionManager.addConnection(this)
        handler.onConnected()
    }

    override fun channelRead0(p0: ChannelHandlerContext, msg: HeliosPacket) {
        msg.handle(handler)
    }


    fun write(packet: HeliosPacket) = ctx.writeAndFlush(packet)

    fun close() = ctx.close()

    fun setHandler(handler: AbstractPacketHandler) {
        this.handler = handler
    }

    override fun handlerAdded(ctx: ChannelHandlerContext) {
        return
    }
}