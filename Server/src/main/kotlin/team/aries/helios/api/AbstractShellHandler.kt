package team.aries.helios.api

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.apache.sshd.common.channel.PtyMode
import org.apache.sshd.server.Environment
import org.apache.sshd.server.ExitCallback
import org.apache.sshd.server.SessionAware
import org.apache.sshd.server.command.Command
import org.apache.sshd.server.session.ServerSession
import org.apache.sshd.server.shell.TtyFilterInputStream
import team.aries.helios.bold
import team.aries.helios.cli.shell.streams.HeliosOutputStream
import team.aries.helios.color16bit
import team.aries.helios.reset
import java.awt.Color
import java.io.*

abstract class AbstractShellHandler : Command, Runnable, SessionAware {

    companion object {
        private const val ENTER = '\r'.toInt()
        private const val CTRL_C = 3
        private const val CTRL_Q = 17
        private const val BACKSPACE = 127
    }

    private lateinit var input: InputStream
    private lateinit var output: HeliosOutputStream
    private lateinit var error: HeliosOutputStream
    private lateinit var exitCallback: ExitCallback
    private lateinit var environment: Environment
    private lateinit var thread: Thread
    private var session: ServerSession? = null
    private var connected = false
    protected var attached = false

    private var aggregated = ""

    private var user: String = "unknown"

    abstract fun onInput(input: String, environment: Environment, exitCallback: ExitCallback): String

    override fun setExitCallback(callback: ExitCallback) { this.exitCallback = callback }

    override fun setInputStream(input: InputStream) { this.input = input }

    override fun start(environment: Environment) {
        this.environment = environment
        this.user = environment.env[Environment.ENV_USER] ?: "anonymous"
        GlobalScope.launch { run() }
    }

    override fun run() {
        connected = true

        output.queueLine("".bold().color16bit(Color.YELLOW))
        output.queueLine("----------------------------------------------------")
        output.queueNewLine()
        output.queueLine("   |    |  |‾‾‾‾‾  |       |   |‾‾‾‾‾|   |‾‾‾‾‾")
        output.queueLine("   |————|  |———    |       |   |     |   |_____")
        output.queueLine("   |    |  |_____  |_____  |   |_____|    _____|")
        output.queueNewLine()
        output.queueLine("----------------------------------------------------\n".reset())
        output.queue("\n\rhelios> ")
        output.flush()

        while(true) {
            val inp = input.read()
            if(inp in 32..126) {
                aggregated += inp.toChar()
                output.writeByte(inp)
            }
            if(inp == ENTER) {
                if(aggregated.isEmpty()) {
                    return
                }
                output.queueNewLine()
                output.queue(onInput(aggregated, environment, exitCallback))
                aggregated = ""
                if(!attached) {
                    output.queue("\r\nhelios> ")
                }
                output.flush()
            }
            if(inp == BACKSPACE) {
                if(aggregated.isNotEmpty()) {
                    aggregated = aggregated.dropLast(1)
                    output.write("\b \b")
                }
            }
            if(inp == CTRL_C) {
                session?.disconnect(1, "Bye bye!")
                destroy()
                break
            }
            if(inp == CTRL_Q) {
                closeAttachedSignal()
            }
        }
    }

    override fun destroy() {
        exitCallback.onExit(0)
        connected = false
    }

    fun write(input: String) {
        if(connected) {
            this.output.write(input)
        }
    }

    abstract fun onDisconnect()

    override fun setSession(session: ServerSession) {
        this.session = session
    }

    abstract fun closeAttachedSignal()

    override fun setErrorStream(error: OutputStream) {this.error = HeliosOutputStream(error) }

    override fun setOutputStream(output: OutputStream) { this.output = HeliosOutputStream(output) }
}