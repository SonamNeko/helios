package team.aries.helios.http

import com.google.gson.Gson
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import team.aries.helios.Helios
import team.aries.helios.http.entity.HttpError
import team.aries.helios.packet.coordbound.Provision
import team.aries.helios.util.ProvisionRequestPipeline

object ServerEndpoints {

    private val mapper = Gson()

    data class ProvisionInfo
    (
        val name: String,
        val rootPackage: String,
        val coordinator: String,
        val variables: Map<String, String>,
        val environment: String
    )

    fun Routing.registerServerEndpoints() {

        get("/api/processes") {
            Helios.connectionManager.connections().asSequence()
                .map { it.processes }
                .flatten().toList().apply {
                    call.respondText(mapper.toJson(this), ContentType.Application.Json)
                }
        }

        get("/api/provisioninfo") {
            call.respond(mutableMapOf<String, Any>(
                "packages" to PackageProviderServer.packages.map {
                    mutableMapOf(
                        "id" to it.id,
                        "variables" to it.variables,
                        "depends" to it.depends
                    )
                },
                "coordinators" to Helios.connectionManager.connections().map { it.name }.toList()
            ))
        }

        post("/api/provision") {
            println("Received provision call")
            val provisionInfo = call.receive<ProvisionInfo>()
            val response = ProvisionRequestPipeline.request(Provision(provisionInfo.name, provisionInfo.rootPackage,
                provisionInfo.environment.split(",").toList(),
                provisionInfo.variables), provisionInfo.coordinator).get()
            if(response.error != null) {
                call.respond(HttpError(response.error))
            } else {
                if(response.provisionResponse != null) {
                    call.respond(object {
                        val success = true
                        val uuid = response.provisionResponse.uuid.toString()
                    })
                } else {
                    call.respond(HttpError("Unknown error"))
                }
            }

        }

    }

}