package team.aries.helios.http.entity

data class HttpError
(
    val error: String
)