package team.aries.helios.http

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.GsonBuilder
import com.typesafe.config.ConfigFactory
import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.config.HoconApplicationConfig
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.HttpsRedirect
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.forEachPart
import io.ktor.http.content.streamProvider
import io.ktor.jackson.jackson
import io.ktor.network.tls.certificates.generateCertificate
import io.ktor.request.header
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.response.respondFile
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.commandLineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.KtorExperimentalAPI
import io.ktor.websocket.WebSockets
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield
import org.slf4j.LoggerFactory
import team.aries.helios.Helios
import team.aries.helios.Properties
import team.aries.helios.api.Package
import team.aries.helios.http.ProcessControlEndpoints.setupProcessControl
import team.aries.helios.http.ServerEndpoints.registerServerEndpoints
import team.aries.helios.http.entity.HttpError
import team.aries.helios.http.ws.WebSocketHandler.setupWebSocket
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.ServerSocket
import java.time.Duration
import java.util.*

object PackageProviderServer {

    @Suppress("PrivatePropertyName")
    private val Application_Json = ContentType.parse("application/json")
    private val packagesDir = File("packages/")
    private val mapper = GsonBuilder().setPrettyPrinting().create()
    val ServerPort = 8500
    private val authKey = String(Base64.getEncoder().encode(Helios.encryptionKey.encoded))
    private val externalKey = System.getProperty("helios.ekey", "debug-key")

    private fun getFreePort(): Int {
        val socket = ServerSocket(0)
        return socket.run {
            this.close()
            this.localPort
        }
    }

    var packages = mutableListOf<team.aries.helios.api.Package>()

    @KtorExperimentalAPI
    fun start() {

        val jksFile = File("build/temporary.jks").apply {
            parentFile.mkdirs()
        }

        if (!jksFile.exists()) {
            generateCertificate(jksFile) // Generates the certificate
        }

        if(!packagesDir.exists()) {
            packagesDir.mkdir()
        }

        packagesDir.listFiles().forEach {
            if(it.isDirectory) {
                val manifest = File(it, "package.json")
                if(manifest.exists()) {
                    val pkg = mapper.fromJson<team.aries.helios.api.Package>(manifest.readText())
                    Helios.logger.info("Loaded package: $pkg")
                    packages.add(pkg)
                }
            }
        }

        Helios.logger.info("Starting HTTP Package Providing Server on port <${Properties.PROVIDER_HOST}:$ServerPort>")
        embeddedServer(Netty, commandLineEnvironment(arrayOf())).start()
    }

    fun Application.main() {
        install(CORS) {
            anyHost()
        }
        install(ContentNegotiation) {
            jackson {  }
        }
        install(WebSockets) {
            pingPeriod = Duration.ofSeconds(15) // Disabled (null) by default
            maxFrameSize = Long.MAX_VALUE // Disabled (max value). The connection will be closed if surpassed this length.
            masking = false
        }
//        install(HttpsRedirect) {
//            sslPort = 8550
//        }
        routing {
            intercept(ApplicationCallPipeline.Call) {
                val key = call.request.header("Auth-Key")
                val isWebSocket = call.request.header("Upgrade") == "websocket"
                if(isWebSocket) {
                    return@intercept
                }
                if((key != authKey) && (key != externalKey)) {
                    println("Provided key: $key")
                    println("Actual key: $authKey")
                    call.request.headers.forEach { s, list ->
                        println("$s :: $list")
                    }
                    call.respond(HttpError("unauthorized"))
                }
            }
            setupWebSocket()
            registerServerEndpoints()
            setupProcessControl()
            get("/packages/all") {
                val mapped = packages.map { it.id }.toList()
                call.respondText(mapper.toJson(mapped), Application_Json, HttpStatusCode.OK)
            }
            get("/package/{id}/{version}") {
                val packageId = call.parameters["id"]
                val pkg = packages.find { it.id == packageId }
                if(pkg == null) {
                    call.respondText("{\"error\": \"package not found\"}", Application_Json, HttpStatusCode.NotFound)
                    return@get
                }
                val packageVersion = if(call.parameters["version"] == "latest") pkg.latest else call.parameters["version"]
                val file = File("packages/$packageId/$packageVersion.zip")
                if(file.exists()) {
                    call.respondFile(file)
                } else {
                    call.respondText("{\"error\": \"file not found\"}", Application_Json, HttpStatusCode.NotFound)
                }
            }
            get("/manifest/{id}") {
                val packageId = call.parameters["id"]
                val pkg = packages.find { it.id == packageId }
                if(pkg != null) {
                    call.respondText(mapper.toJson(pkg), Application_Json, HttpStatusCode.OK)
                } else {
                    call.respondText("{\"error\": \"file not found\"}", Application_Json, HttpStatusCode.NotFound)
                }
            }
            get("/daemon") {
                call.respondFile(File("packages/ProcessDaemon.jar"))
            }
            post("/package/{id}/{version}") {
                val multipart = call.receiveMultipart()
                val packageId = call.parameters["id"]
                val packageVersion = call.parameters["version"]
                multipart.forEachPart { part ->
                    when(part) {
                        is PartData.FileItem -> {
                            val extension = File(part.originalFileName).extension
                            if(extension != "zip") {
                                call.respondText("{\"error\": \"only .zip files allowed\"}")
                                return@forEachPart
                            }
                            val directory = File("packages/$packageId/")
                            val created = directory.mkdirs()
                            if(created) {
                                File(directory, "package.json").apply {
                                    this.createNewFile()
                                    this.writeText(mapper.toJson(Package(packageId!!, mutableListOf(packageVersion!!), packageVersion).apply {
                                        Helios.logger.info("New package: $this")
                                        packages.add(this)
                                    }))
                                }
                            }
                            val file = File(directory, "$packageVersion.zip")
                            if(file.exists()) {
                                file.delete()
                            }
                            file.createNewFile()
                            part.streamProvider().use { input -> file.outputStream().buffered().use { output -> input.copyToSuspend(output) } }
                        }
                    }
                }
            }
        }
    }

    suspend fun InputStream.copyToSuspend(
        out: OutputStream,
        bufferSize: Int = DEFAULT_BUFFER_SIZE,
        yieldSize: Int = 4 * 1024 * 1024,
        dispatcher: CoroutineDispatcher = Dispatchers.IO
    ): Long {
        return withContext(dispatcher) {
            val buffer = ByteArray(bufferSize)
            var bytesCopied = 0L
            var bytesAfterYield = 0L
            while (true) {
                val bytes = read(buffer).takeIf { it >= 0 } ?: break
                out.write(buffer, 0, bytes)
                if (bytesAfterYield >= yieldSize) {
                    yield()
                    bytesAfterYield %= yieldSize
                }
                bytesCopied += bytes
                bytesAfterYield += bytes
            }
            return@withContext bytesCopied
        }
    }

}