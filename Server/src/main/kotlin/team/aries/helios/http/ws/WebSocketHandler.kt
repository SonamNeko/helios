package team.aries.helios.http.ws

import com.google.gson.Gson
import io.ktor.application.call
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.close
import io.ktor.http.cio.websocket.readText
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.websocket.WebSocketServerSession
import io.ktor.websocket.webSocket
import team.aries.helios.Helios
import team.aries.helios.Properties
import team.aries.helios.http.PackageProviderServer
import team.aries.helios.http.entity.HttpError
import team.aries.helios.util.generateString

object WebSocketHandler {

    private val mapper = Gson()

    val IdMapping = mutableMapOf<String, String>()
    val AttachMapping = mutableMapOf<WebSocketServerSession, AttachWsProcess>()

    fun Routing.setupWebSocket() {
        webSocket("/api/attachprocess") {
            incomingLoop@ while(true) {
                val frame = incoming.receive()
                when(frame) {
                    is Frame.Text -> {
                        val input = frame.readText()
                        if(input.startsWith("ATTACH::")) {
                            val id = input.split("::")
                            if(id.size == 1 || !IdMapping.containsKey(id[1])) {
                                this@webSocket.send(Frame.Text("Could not find attach ID"))
                                this@webSocket.close(CloseReason(CloseReason.Codes.UNEXPECTED_CONDITION, "Could not find attach ID (${id[1]})"))
                            } else {
                                val processId = IdMapping[id[1]]!!
                                AttachMapping[this] = AttachWsProcess(processId) {
                                    this@webSocket.send(Frame.Text("OUTPUT::$it"))
                                }
                            }
                        }
                        if(input.startsWith("INPUT::")) {
                            val inp = input.split("::")[1]
                            AttachMapping[this]?.sendInput(inp)
                        }
                        if(input == "END_CONN") {
                            IdMapping.remove(AttachMapping[this]?.processUuid)
                            AttachMapping[this]?.closeConnection()
                            AttachMapping.remove(this)
                            this@webSocket.close(CloseReason(CloseReason.Codes.NORMAL, "Bye bye!"))
                            break@incomingLoop
                        }
                    }
                }
            }
        }

        get("/api/reqattach/{id}") {
            val serverId = call.parameters["id"]
            if(serverId == null || !Helios.connectionManager.connections().any { it.processes.any { p -> p.uuid.toString() == serverId } }) {
                call.respond(HttpError("Process by id <$serverId> not found"))
            } else {
                val connectId = generateString()
                IdMapping[connectId] = serverId
                call.respond(mutableMapOf(
                    "attachUri" to "ws://${Properties.HOST}:${PackageProviderServer.ServerPort}/api/attachprocess",
                    "attachId" to connectId
                ))
            }
        }
    }

}