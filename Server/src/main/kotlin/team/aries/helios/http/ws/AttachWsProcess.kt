package team.aries.helios.http.ws

import team.aries.helios.util.AttachedProcessOutput

class AttachWsProcess(val processUuid: String, private var consumer: suspend (String) -> Unit) {

    private val attachedProcessOutput: AttachedProcessOutput = AttachedProcessOutput(processUuid) {
        consumer(it)
    }

    init {
        attachedProcessOutput.start()
    }

    fun sendInput(string: String) {
        attachedProcessOutput.writeInput(string)
    }

    fun closeConnection() {
        attachedProcessOutput.stop()
    }

}