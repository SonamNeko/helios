package team.aries.helios.http

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.post
import team.aries.helios.Helios
import team.aries.helios.api.CoordProcess
import team.aries.helios.packet.coordbound.Command

object ProcessControlEndpoints {

    fun Routing.setupProcessControl() {
        post("/api/process/{id}/stop") {
            val id = call.parameters["id"]!!
            Helios.connectionManager.connections()
                .find { it.processes.any { process -> process.uuid.toString() == id } }
                ?.write(Command("STOP", listOf(id)))
            call.respond(object {})
        }
        post("/api/process/{id}/start") {
            val id = call.parameters["id"]!!
            Helios.connectionManager.connections()
                .find { it.processes.any { process -> process.uuid.toString() == id && process.status == CoordProcess.Status.NOT_RUNNING } }
                ?.write(Command("START", listOf(id)))
            call.respond(object {})
        }
        post("/api/process/{id}/restart") {
            val id = call.parameters["id"]!!
            Helios.connectionManager.connections()
                .find { it.processes.any { process -> process.uuid.toString() == id } }
                ?.write(Command("START", listOf(id)))
            call.respond(object {})
        }
    }

}