package team.aries.helios.handler

import com.google.gson.JsonObject
import team.aries.helios.Helios
import team.aries.helios.PipelineUtils
import team.aries.helios.Properties
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosCoordinator
import team.aries.helios.packet.Negotiate
import team.aries.helios.packet.NegotiateAck
import team.aries.helios.packet.coordbound.AuthResponse
import team.aries.helios.packet.serverbound.Auth
import team.aries.helios.packet.serverbound.Synchronize
import java.security.PublicKey
import java.util.*
import javax.crypto.Cipher

class NotAuthInboundHandler(private val coordinator: HeliosCoordinator) : AbstractPacketHandler() {

    override fun handle(negotiate: Negotiate) {
        if(negotiate.meetsCriteria) {
            this.coordinator.publicKey = negotiate.publicKey
            val cipher = Cipher.getInstance("RSA")
            cipher.init(Cipher.ENCRYPT_MODE, coordinator.publicKey)
            val encrypted = cipher.doFinal(coordinator.encryptionKey.encoded)
            coordinator.write(NegotiateAck(true, encrypted))
            PipelineUtils.setupEncryption(this.coordinator.ctx.pipeline(), coordinator.encryptionKey)
        } else {
            coordinator.close()
        }
    }

    override fun handle(auth: Auth) {
        if(this.coordinator.publicKey == null) {
            coordinator.close()
            return
        }
        if(Properties.AUTH_KEY == null || auth.authKey.contentEquals(Properties.AUTH_KEY.toByteArray())) {

            val config = JsonObject().apply {
                addProperty("providerPort", Helios.providerServer.ServerPort)
                addProperty("providerHost", Properties.PROVIDER_HOST)
            }

            this.coordinator.name = auth.name
            this.coordinator.authenticated = true

            coordinator.setHandler(HeliosInboundHandler(this.coordinator).apply {
                this.coordinator.ctx.channel().pipeline().apply {
                    coordinator.write(AuthResponse(true, config))
                }
                onConnected()
            })
        } else {
            coordinator.write(AuthResponse(false))
        }
    }

    override fun onConnected() {}

    override fun onDisconnected() {}

}