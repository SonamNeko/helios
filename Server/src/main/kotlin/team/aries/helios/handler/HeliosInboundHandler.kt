package team.aries.helios.handler

import team.aries.helios.Helios
import team.aries.helios.Protocol
import team.aries.helios.api.AbstractPacketHandler
import team.aries.helios.api.HeliosCoordinator
import team.aries.helios.color16bit
import team.aries.helios.packet.Output
import team.aries.helios.packet.serverbound.Config
import team.aries.helios.packet.serverbound.ProvisionResponse
import team.aries.helios.packet.serverbound.Synchronize
import team.aries.helios.reset
import team.aries.helios.util.ProvisionRequestPipeline
import java.awt.Color

class HeliosInboundHandler(val coordinator: HeliosCoordinator) : AbstractPacketHandler() {

    override fun handle(config: Config) {
        Helios.logger.info("[${coordinator.name}] Received Config ${Protocol.MAPPER.toJson(config)}")
        coordinator.configuration = config
    }

    override fun handle(output: Output) {
        Helios.logger.info("[${coordinator.name}] ${output.text}")
    }

    override fun handle(synchronize: Synchronize) {
        coordinator.processes.clear()
        coordinator.processes.addAll(synchronize.processes)
    }

    override fun handle(provisionRes: ProvisionResponse) {
        ProvisionRequestPipeline.respond(provisionRes)
    }

    override fun onConnected() {
        val channel = coordinator.ctx.channel()
        Helios.logger.info("Coordinator \'${coordinator.name.color16bit(Color.YELLOW).reset()}\' connected from <${channel.remoteAddress()}>")
    }

    override fun onDisconnected() {
        Helios.logger.info("Coordinator \'${coordinator.name.color16bit(Color.YELLOW).reset()}\' disconnected")
    }

}