package team.aries.helios

object Properties {

    val HOST = System.getProperty("helios.host", "localhost")
    val PORT = System.getProperty("helios.port", "4950").toIntOrNull() ?: 4950
    val PROVIDER_HOST = System.getProperty("helios.provd", "localhost")
    val AUTH_KEY = System.getenv("AUTH_KEY")
    val EXTERNAL_LEY = System.getenv("EXT_KEY")
}