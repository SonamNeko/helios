package team.aries.helios.util

import java.util.concurrent.ThreadLocalRandom
import kotlin.streams.asSequence

private val Chars = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM$#@"

fun generateString(length: Int = 16): String {
    return ThreadLocalRandom.current()
        .ints(length.toLong(), 0, Chars.length)
        .asSequence()
        .map(Chars::get)
        .joinToString("")
}