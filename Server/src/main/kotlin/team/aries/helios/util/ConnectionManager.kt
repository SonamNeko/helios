package team.aries.helios.util

import io.prometheus.client.Gauge
import team.aries.helios.api.HeliosCoordinator
import java.util.concurrent.ConcurrentLinkedDeque

class ConnectionManager {

    private val connections = ConcurrentLinkedDeque<HeliosCoordinator>()

    companion object {
        val ConnectionsMetric = Gauge
            .build()
            .name("connections_total")
            .help("Total Connections")
            .register()
    }

    fun addConnection(connection: HeliosCoordinator) {
        ConnectionsMetric.inc()
        connections.add(connection)
    }

    fun removeConnection(connection: HeliosCoordinator) {
        ConnectionsMetric.dec()
        connections.remove(connection)
    }



    fun connections() = connections

}