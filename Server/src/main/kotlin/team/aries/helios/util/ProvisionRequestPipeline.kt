package team.aries.helios.util

import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.RemovalCause
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import team.aries.helios.Helios
import team.aries.helios.packet.coordbound.Provision
import team.aries.helios.packet.coordbound.ProvisionRequest
import team.aries.helios.packet.serverbound.ProvisionResponse
import java.time.Duration
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future

object ProvisionRequestPipeline {

    data class PipelineResponse(
        val provisionResponse: ProvisionResponse?,
        val error: String?
    )

    private val requestMap = Caffeine.newBuilder()
        .maximumSize(10_000)
        .expireAfterWrite(Duration.ofSeconds(10))
        .removalListener<String, CompletableFuture<PipelineResponse>> { _, value, removalCause ->
            if(removalCause == RemovalCause.EXPIRED) {
                GlobalScope.launch {
//                    value?.invoke(null, "Request timeout")
                }
            }
        }
        .build<String, CompletableFuture<PipelineResponse>>()

    fun request(provision: Provision, coordinator: String): Future<PipelineResponse> {
        val future = CompletableFuture<PipelineResponse>()
        println("Requesting provision for $coordinator")
        Helios.connectionManager.connections().run {
            if(coordinator != "any") {
                this.find { it.name == coordinator }
            } else {
                this.random()
            }
        }?.apply {
            println("Sending provision request to coordinator ${this.name}")
        }?.write(ProvisionRequest(provision).apply {
                requestMap.put(this.ackId, future)
            })
        return future
    }

    fun respond(provisionResponse: ProvisionResponse) {
        println("Response :: ${provisionResponse.ackId} :: ${provisionResponse.uuid}")
        requestMap.getIfPresent(provisionResponse.ackId)?.complete(PipelineResponse(provisionResponse, null))
        requestMap.invalidate(provisionResponse.ackId)
    }

}