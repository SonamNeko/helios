package team.aries.helios.util

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInitializer
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollSocketChannel
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.Delimiters
import io.netty.handler.codec.LengthFieldBasedFrameDecoder
import io.netty.handler.codec.LengthFieldPrepender
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import team.aries.helios.Helios
import team.aries.helios.netty.AESDecoder
import team.aries.helios.netty.AESEncoder
import java.util.concurrent.Executors

class AttachedProcessOutput(private val processUuid: String, val outputConsumer: suspend (String) -> Unit) {

    private lateinit var ctx: ChannelHandlerContext
    private val dispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    fun start() {
        Helios.connectionManager.connections()
            .find { it.processes.any { process -> process.uuid.toString() == processUuid } }?.let { coordinator ->
                GlobalScope.launch {
                    Bootstrap()
                        .channel(EpollSocketChannel::class.java)
                        .group(EpollEventLoopGroup())
                        .handler(object: ChannelInitializer<SocketChannel>() {
                            override fun initChannel(chn: SocketChannel) {
                                chn.pipeline()
                                    .addLast(LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4))
                                    .addLast(LengthFieldPrepender(4))
                                    .addLast(AESDecoder(Helios.encryptionKey))
                                    .addLast(AESEncoder(Helios.encryptionKey))
                                    .addLast(DelimiterBasedFrameDecoder(2048, *Delimiters.nulDelimiter()))
                                    .addLast(StringDecoder())
                                    .addLast(StringEncoder())
                                    .addLast(object: SimpleChannelInboundHandler<String>() {
                                        override fun channelActive(ctx: ChannelHandlerContext) {
                                            this@AttachedProcessOutput.ctx = ctx
                                            ctx.channel().writeAndFlush("attach $processUuid\u0000")
                                        }

                                        override fun channelInactive(ctx: ChannelHandlerContext?) {
                                            GlobalScope.launch(dispatcher) {
                                                outputConsumer("Process ended")
                                            }
                                        }

                                        override fun channelRead0(ctx: ChannelHandlerContext, msg: String) {
                                            GlobalScope.launch(dispatcher) {
                                                outputConsumer(msg)
                                            }
                                        }
                                    })
                            }
                        })
                        .connect(coordinator.configuration!!.hostTunnel, coordinator.configuration!!.portTunnel).addListener { future ->
                            if(!future.isSuccess) {
                                GlobalScope.launch(dispatcher) {
                                    outputConsumer("Could not connect to coordinator ${coordinator.name}")
                                }
                            }
                        }.channel().closeFuture().sync()
                }
            }
    }

    fun writeInput(string: String) {
        ctx.writeAndFlush("$string\u0000")
    }

    fun stop() {
        ctx.close()
    }

}