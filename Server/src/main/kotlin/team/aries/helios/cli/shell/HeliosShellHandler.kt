package team.aries.helios.cli.shell

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInitializer
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollSocketChannel
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.Delimiters
import io.netty.handler.codec.LengthFieldBasedFrameDecoder
import io.netty.handler.codec.LengthFieldPrepender
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import org.apache.sshd.server.Environment
import org.apache.sshd.server.ExitCallback
import team.aries.helios.Helios
import team.aries.helios.api.AbstractShellHandler
import team.aries.helios.api.HeliosCoordinator
import team.aries.helios.api.Package
import team.aries.helios.color16bit
import team.aries.helios.netty.AESDecoder
import team.aries.helios.netty.AESEncoder
import team.aries.helios.reset
import java.awt.Color
import java.lang.Exception
import java.util.*

class HeliosShellHandler : AbstractShellHandler() {

    private var attachedCtx: ChannelHandlerContext? = null

    private val spacing = "   "

    override fun onInput(input: String, environment: Environment, exitCallback: ExitCallback): String {
        val split = input.split(" ")
        val cmd = split[0].toLowerCase()
        val args = if(split.size == 1) emptyList() else split.drop(1)

        if(!attached) {

            when(cmd) {
                "list", "l" -> {
                    Helios.connectionManager.connections().filter { it.authenticated }.forEachIndexed { index, coordinator ->
                        write("Coordinator ${index.toString().color16bit(Color.YELLOW).reset()}\n\r$spacing> ID ${coordinator.name}\n\r$spacing (Running ${coordinator.processes.size} processes)")
                        coordinator.processes.forEach {
                            write("\n\r$spacing ${it.name}::${it.uuid} \'${it.rootPackage}\'")
                        }
                    }
                    return "\n\n"
                }
                "connect" -> {
                    if(args.isEmpty()) {
                        return "Usage: connect <coordinator>".color16bit(Color.RED).reset()
                    }

                    val heliosCoordinator: HeliosCoordinator? = if(args[0].toIntOrNull() != null) {
                        Helios.connectionManager.connections().elementAt(args[0].toInt())
                    } else {
                        Helios.connectionManager.connections().find { it.name == args[0] && it.authenticated && it.configuration != null }
                    }

                    heliosCoordinator?.let {
                        Thread {
                            newProxyClient(it)
                        }.start()
                    } ?: write("Could not find process / associated coordinator")
                    return ""
                }
                "attach" -> {
                    if(args.isEmpty()) {
                        return "Usage: attach <process uuid | name>".color16bit(Color.RED).reset()
                    }

                    val heliosCoordinator: HeliosCoordinator? = try {
                        Helios.connectionManager.connections().find { it.processes.any { p -> p.uuid == UUID.fromString(args[0]) } }
                    } catch (ex: Exception) {
                        Helios.connectionManager.connections().find { it.processes.any { p -> p.name == args[0] } }
                    }

                    heliosCoordinator?.let {
                        Thread {
                            newProxyClient(it, "attach ${args[0]}")
                        }.start()
                    } ?: write("Could not find process / associated coordinator")
                    return ""
                }
                "package", "pkg" -> {
                    if(args.isEmpty()) {
                        return """
                        Usage: package [-all][-a][-rm]
                            ${"-all".color16bit(Color.YELLOW).reset()} Show all current packages
                            ${"-a <package name>".color16bit(Color.YELLOW).reset()} Add new package
                            ${"-rm <package name>".color16bit(Color.YELLOW).reset()} Remove package
                            ${"-p <package name> -v <version>".color16bit(Color.YELLOW).reset()} Promote 'version' to latest
                    """.trimIndent().color16bit(Color.RED).reset()
                    }
                    if(args.indexOf("-all") != -1) {
                        write("\nID\t\t\tVERSIONS\t\t\tLATEST".color16bit(Color.YELLOW).reset())
                        Helios.providerServer.packages.forEach {
                            write("${it.id}\t\t${it.versions}\t\t\t${it.latest}")
                        }
                        return "\n"
                    }
                    if(args.indexOf("-a") != -1) {
                        val index = args.indexOf("-a")
                        if(args.size == (index + 1)) {
                            return "-a flag MUST contain new package name".color16bit(Color.RED).reset()
                        }
                        val packageName = args[index + 1].toLowerCase()
                        if(Helios.providerServer.packages.any { it.id == packageName }) {
                            return "Package with id \'$packageName\' already exists".color16bit(Color.RED).reset()
                        }
                        Helios.providerServer.packages.add(Package(packageName))
                        return "Created new package with id \'$packageName\'".color16bit(Color(102,255,91)).reset()
                    }
                    return ""
                }
                "whitelist", "wl" -> {
                    if(args.isEmpty()) {

                    }
                }
            }
        } else {
            attachedCtx!!.channel().writeAndFlush("$input\u0000")
            return ""
        }
        return "Command not found"
    }

    override fun closeAttachedSignal() {
        attachedCtx?.close()
        attached = false
    }

    override fun onDisconnect() {
        attachedCtx?.close()
        attached = false
    }

    private fun newProxyClient(coordinator: HeliosCoordinator, initCommand: String? = null) {
        Bootstrap()
            .channel(EpollSocketChannel::class.java)
            .group(EpollEventLoopGroup())
            .handler(object: ChannelInitializer<SocketChannel>() {
                override fun initChannel(chn: SocketChannel) {
                    chn.pipeline()
                        .addLast(LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4))
                        .addLast(LengthFieldPrepender(4))
                        .addLast(AESDecoder(coordinator.encryptionKey))
                        .addLast(AESEncoder(coordinator.encryptionKey))
                        .addLast(DelimiterBasedFrameDecoder(2048, *Delimiters.nulDelimiter()))
                        .addLast(StringDecoder())
                        .addLast(StringEncoder())
                        .addLast(object: SimpleChannelInboundHandler<String>() {
                            override fun channelActive(ctx: ChannelHandlerContext) {
                                attached = true
                                attachedCtx = ctx
                                write("Connected to coordinator ${coordinator.name}\n\r\n\n")
                                if(initCommand != null) {
                                    ctx.channel().writeAndFlush("$initCommand\u0000")
                                }
                            }

                            override fun channelInactive(ctx: ChannelHandlerContext?) {
                                attached = false
                                attachedCtx = null
                                write("Connection with ${coordinator.name} was ended\r\nhelios> ")
                            }

                            override fun channelRead0(ctx: ChannelHandlerContext, msg: String) {
                                write(msg)
                            }
                        })
                }
            })
            .connect(coordinator.configuration!!.hostTunnel, coordinator.configuration!!.portTunnel).addListener { future ->
                if(!future.isSuccess) {
                    write("Could not connect to coordinator ${coordinator.name}\r\n")
                }
            }.channel().closeFuture().sync()
    }

}