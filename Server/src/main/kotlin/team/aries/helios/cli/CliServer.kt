package team.aries.helios.cli

import org.apache.logging.log4j.LogManager
import org.apache.sshd.common.config.keys.impl.RSAPublicKeyDecoder
import org.apache.sshd.server.SshServer
import org.apache.sshd.server.auth.password.PasswordAuthenticator
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider
import team.aries.helios.Helios
import team.aries.helios.cli.shell.HeliosShellFactory
import java.io.File
import java.nio.file.Paths
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.RSAPublicKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*

class CliServer {

    private val publicKeys = mutableListOf<PublicKey>()
    private val logger = LogManager.getLogger(this::class.java)

    fun start() {
        loadPublicKeys()
        val server = SshServer.setUpDefaultServer()
        server.port = 5000
        server.keyPairProvider = SimpleGeneratorHostKeyProvider(Paths.get("hostkey.ser"))
        server.publickeyAuthenticator = PublickeyAuthenticator { _, publicKey, _ -> publicKeys.any { it.encoded!!.contentEquals(publicKey.encoded) } }
        server.shellFactory = HeliosShellFactory()
        server.start()
    }

    private fun loadPublicKeys() {
        File("keys/").apply {
            if(this.exists()) this.mkdir()
            var count = 0
            this.listFiles().forEach {
                count++
                val key = it.readText().split(" ")[1].toByteArray()
                val spec = RSAPublicKeyDecoder()
                val pub = spec.decodePublicKey(null, "RSA", *Base64.getDecoder().decode(key))
                publicKeys.add(pub)
                print("\rLoading $count keys...")
            }
            print("\r")
            logger.info("[SSH] Loaded $count public keys")
        }
    }

}