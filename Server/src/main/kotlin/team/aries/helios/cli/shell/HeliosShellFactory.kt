package team.aries.helios.cli.shell

import org.apache.sshd.server.command.Command
import org.apache.sshd.server.shell.ShellFactory

class HeliosShellFactory : ShellFactory {

    override fun create(): Command {
        return HeliosShellHandler()
    }

}