package team.aries.helios.cli.shell.streams

import java.io.OutputStream

class HeliosOutputStream(val outputStream: OutputStream) {

    fun writeLine(string: String): HeliosOutputStream {
        queueLine(string)
        flush()
        return this
    }

    fun writeByte(int: Int) {
        outputStream.write(int)
        flush()
    }

    fun write(string: String): HeliosOutputStream {
        queue(string)
        flush()
        return this
    }

    fun newLine(): HeliosOutputStream {
        queueNewLine()
        flush()
        return this
    }

    fun queueLine(string: String): HeliosOutputStream {
        outputStream.write("$string\n\r".toByteArray())
        return this
    }
    fun queue(string: String): HeliosOutputStream {
        outputStream.write(string.toByteArray())
        return this
    }

    fun queueNewLine(): HeliosOutputStream {
        outputStream.write("\n\r".toByteArray())
        return this
    }

    fun flush() = outputStream.flush()

}