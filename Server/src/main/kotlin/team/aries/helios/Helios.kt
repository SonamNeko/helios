package team.aries.helios

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.*
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollServerSocketChannel
import io.prometheus.client.exporter.HTTPServer
import io.prometheus.client.hotspot.DefaultExports
import org.apache.logging.log4j.LogManager
import team.aries.helios.api.HeliosCoordinator
import team.aries.helios.cli.CliServer
import team.aries.helios.http.PackageProviderServer
import team.aries.helios.util.ConnectionManager
import java.awt.Color
import java.util.concurrent.Future
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

object Helios {

    private var starting = false
    val logger = LogManager.getLogger(this::class.java)
    lateinit var encryptionKey: SecretKey
    val connectionManager = ConnectionManager()
    lateinit var providerServer: PackageProviderServer

    fun start() {

        println("""
            *----------------------------------------------------
            *
            *    |    |  |‾‾‾‾‾  |       |   |‾‾‾‾‾|   |‾‾‾‾‾
            *    |————|  |———    |       |   |     |   |_____
            *    |    |  |_____  |_____  |   |_____|    _____|
            *
            *             Server Management System
            *                  Aries Network
            *                   Version 1.1
            *
            *----------------------------------------------------
        """.trimMargin("*").color16bit(Color(239,222,153)).bold().reset())

        if(starting) return
        starting = true
        initialize()
    }

    private fun initialize() {
        generateRsaKeyPair()
        startHttpProviderServer()
        startCliInterface()
        metricsServer()
        startHeliosServer()
    }

    private fun startHttpProviderServer() {
        providerServer = PackageProviderServer
        providerServer.start()
    }

    private fun generateRsaKeyPair() {
        logger.info("Generating AES Encryption Key...")
        val generator = KeyGenerator.getInstance("AES")
        generator.init(256)
        encryptionKey = generator.generateKey()
        logger.info("AES Encryption Key Generated")
    }

    private fun metricsServer() {
        DefaultExports.initialize()
        HTTPServer(9800).apply {
            println(this.port)
        }
    }

    private fun startCliInterface() {
        Thread {
            CliServer().start()
        }.start()
    }

    private fun startHeliosServer() {
        logger.info("Starting Helios Server on [${Properties.HOST}:${Properties.PORT}]")
        ServerBootstrap()
            .channel(EpollServerSocketChannel::class.java)
            .group(EpollEventLoopGroup(), EpollEventLoopGroup())
            .childOption(ChannelOption.AUTO_READ, true)
            .childOption(ChannelOption.TCP_NODELAY, true)
            .childHandler(object: ChannelInitializer<Channel>() {
                override fun initChannel(ch: Channel) {
                    ch.pipeline().apply {
                        PipelineUtils.setupPipeline(this, HeliosCoordinator())
                    }
                }
            }).bind(Properties.HOST, Properties.PORT)
            .addListener {
                if(it.isSuccess) {
                    logger.info("Helios Server listening on [${Properties.HOST}:${Properties.PORT}]")
                }
            }
            .channel().closeFuture().sync()
    }

}